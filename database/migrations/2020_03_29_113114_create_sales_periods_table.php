<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_period', function (Blueprint $table) {
            $table->id();
            $table->date('period_start_date');
            $table->date('period_end_date');
            $table->char('is_period_close', 1)->default(0);	
            $table->string('branch', 250)->nullable();
            $table->string('branch_code', 50)->nullable();
            $table->string('shift', 100)->nullable();
            $table->string('shift_code', 50)->nullable();
            $table->string('created_by',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_period');
    }
}
