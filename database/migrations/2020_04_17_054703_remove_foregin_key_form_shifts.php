<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveForeginKeyFormShifts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shifts', function (Blueprint $table) {
            
            if (Schema::hasColumn('shifts', 'sales_period_id')){
                $table->dropForeign('shifts_sales_period_id_foreign');
               $table->dropColumn('sales_period_id');
            }
            if (Schema::hasColumn('shifts', 'attendance_period_id')){
                $table->dropForeign('shifts_attendance_period_id_foreign');
               $table->dropColumn('attendance_period_id');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shifts', function (Blueprint $table) {
            //
        });
    }
}
