<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBranchKeyToAttPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_periods', function (Blueprint $table) {
            $table->dropColumn('branch');
            $table->dropColumn('branch_code');
            $table->dropColumn('shift');
            $table->dropColumn('shift_code');
            $table->unsignedBigInteger('branch_id')->after('is_period_close');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->string('type',10)->after('is_period_close');
            $table->string('period_key',100)->after('is_period_close');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_periods', function (Blueprint $table) {
            $table->dropForeign('attendance_periods_branch_id_foreign');
            $table->dropColumn('branch_id');
            $table->dropColumn('type');
            $table->dropColumn('period_key');
        });
    }
}
