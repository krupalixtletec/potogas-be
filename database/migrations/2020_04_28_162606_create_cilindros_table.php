<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCilindrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cilindros', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('employee_name',250)->nullable();
            $table->date('date')->nullable();
            $table->string('route_code',250)->nullable();
            $table->string('activity_code',250)->nullable();
            $table->string('branch_code',250)->nullable();
            $table->string('truck_number',250)->nullable();
            $table->string('destajo_type',250)->nullable();
            $table->integer('capture_kilos')->nullable();
            $table->string('number_of_cilindros',250)->nullable();
            $table->string('exported_to_processo',1)->nullable();
            $table->string('created_by',100)->nullable();
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cilindros');
    }
}
