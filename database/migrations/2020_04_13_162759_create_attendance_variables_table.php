<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_variables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('att_period_id');
            $table->foreign('att_period_id')->references('id')->on('attendance_periods');
            $table->unsignedBigInteger('shifts_mapping_id');
            $table->foreign('shifts_mapping_id')->references('id')->on('shift_branch_mappings');
            $table->integer('employee_id')->nullable();
            $table->string('employee_code',250)->nullable();
            $table->string('employee_name',250)->nullable();
            $table->string('ruta_fija',100)->nullable();
            $table->string('ruta_fija_code',100)->nullable();
            $table->string('puesto',250)->nullable();
            $table->date('att_date')->nullable();
            $table->string('ruta_code',100)->nullable();
            $table->string('actividad_code',100)->nullable();
            $table->string('incidencia_code',100)->nullable();
            $table->string('other_shift_code',100)->nullable();
            $table->string('oth_shift_actividad_code',100)->nullable();
            $table->char('is_freezed',1)->default(0);
            $table->string('created_by',250)->nullable();
            $table->string('updated_by',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_variables');
    }
}
