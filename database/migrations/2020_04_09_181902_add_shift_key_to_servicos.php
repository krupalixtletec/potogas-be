<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShiftKeyToServicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicos_sales_details', function (Blueprint $table) {
            $table->unsignedBigInteger('shifts_mapping_id')->after('id');
            $table->foreign('shifts_mapping_id')->references('id')->on('shift_branch_mappings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicos_sales_details', function (Blueprint $table) {
            //
        });
    }
}
