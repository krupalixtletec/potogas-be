<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBranchIdToSalesPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_period', function (Blueprint $table) {
            $table->dropColumn('branch');
            $table->dropColumn('branch_code');
            $table->dropColumn('shift');
            $table->dropColumn('shift_code');
            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches')->after('is_period_close');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_period', function (Blueprint $table) {
            //
        });
    }
}
