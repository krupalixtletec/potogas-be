<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicosSalesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicos_sales_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sales_period_id');
            $table->foreign('sales_period_id')->references('id')->on('sales_period');			
            $table->date('sale_date');
            $table->string('ruta', 100)->nullable();	
            $table->string('ruta_code', 50)->nullable();
            $table->double('sale_val', 10, 2)->nullable();
            $table->char('is_freezed',1)->default(0);
            $table->string('created_by',250)->nullable();
            $table->string('updated_by',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicos_sales_details');
    }
}
