<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncapacidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incapacidades', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('employee_name',250)->nullable();
            $table->string('numero_de_folio',250)->nullable();
            $table->date('start_date')->nullable();
            $table->date('discount_date')->nullable();
            $table->integer('duration')->nullable();
            $table->string('rama',250)->nullable();
            $table->string('risk_type',250)->nullable();
            $table->string('consecuence',250)->nullable();
            $table->string('control',250)->nullable();
            $table->date('date_of_accident')->nullable();
            $table->string('place_of_accident',250)->nullable();
            $table->string('accident_cause',250)->nullable();
            $table->string('type_of_injury',250)->nullable();
            $table->string('corrective_measure',250)->nullable();
            $table->tinyInteger('day')->nullable();
            $table->string('time',250)->nullable();
            $table->string('created_by',100)->nullable();
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incapacidades');
    }
}
