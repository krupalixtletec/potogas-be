<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceFijosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_fijos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('att_period_id');
            $table->foreign('att_period_id')->references('id')->on('attendance_periods');
            $table->unsignedBigInteger('shifts_mapping_id');
            $table->foreign('shifts_mapping_id')->references('id')->on('shift_branch_mappings');
            $table->integer('employee_id')->nullable();
            $table->string('employee_code',250)->nullable();
            $table->string('employee_name',250)->nullable();
            $table->string('puesto',250)->nullable();
            $table->date('att_date')->nullable();
            $table->string('incidencia_code',100)->nullable();
            $table->integer('tiempo_extra')->nullable();
            $table->char('viaticos',1)->nullable();
            $table->char('is_freezed',1)->default(0);
            $table->string('created_by',250)->nullable();
            $table->string('updated_by',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_fijos');
    }
}
