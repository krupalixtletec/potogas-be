<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class userAndRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_roles')->insert([
            ['role_code' => 'admin', 'role_name' => 'Admin']
        ]);

        DB::table('users')->insert([
            ['user_id' => 1, 'name' => 'Admin', 'email' => 'admin@gmail.com']
        ]);

        DB::table('users_role_mapping')->insert([
            ['user_id' => 1, 'role_id' => 1]
        ]);

        DB::table('user_branch_mappings')->insert([
            ['user_id' => 1, 'branch_id' => 9],
            ['user_id' => 1, 'branch_id' => 16]
        ]);

    }
}
