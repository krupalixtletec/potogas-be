<?php

use Illuminate\Database\Seeder;

class branchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('branches')->insert([
            ['branch_code' => '101-S', 'branch_name' => 'Carburadora San Pedro (Sindicato)'],
            ['branch_code' => '102-S', 'branch_name' => 'Carburadora San Felipe (Sindicato)'],
            ['branch_code' => '103-S', 'branch_name' => 'Carburadora Soledad (Sindicato)'],
            ['branch_code' => '104-S', 'branch_name' => 'Carburadora Mexquitic (Sindicato)'],
            ['branch_code' => '105-S', 'branch_name' => 'Carburadora Oriente (Sindicato)'],
            ['branch_code' => '106-S', 'branch_name' => 'Carburadora Poniente (Sindicato)'],
            ['branch_code' => '107-S', 'branch_name' => 'Carburadora Santa Maria (Sindicato)'],
            ['branch_code' => '108-S', 'branch_name' => 'Carburadora Villa Hidalgo (Sindicato)'],
            ['branch_code' => '109-S', 'branch_name' => 'Planta Salinas (Sindicato)'],
            ['branch_code' => '110-S', 'branch_name' => 'Planta Cd Valles (Sindicato)'],
            ['branch_code' => '111-S', 'branch_name' => 'Planta San Luis Potosí (Sindicato)'],
            ['branch_code' => '202-S', 'branch_name' => 'Planta Cardenas_DPG (Sindicato)'],
            ['branch_code' => '301-S', 'branch_name' => 'Carburadora El Naranjo (Sindicato)'],
            ['branch_code' => '401-S', 'branch_name' => 'Planta Rioverde - San Luis Gas (Sindicato)'],
            ['branch_code' => '402-S', 'branch_name' => 'Planta Rioverde - DPG (Sindicato)'],
            ['branch_code' => '501-S', 'branch_name' => 'Planta Matehuala - San Luis Gas (Sindicato)'],
            ['branch_code' => '601-S', 'branch_name' => 'Planta Cedral CORPO (sindicato)'],
            ['branch_code' => '701-S', 'branch_name' => 'Planta Matehuala San Diego Gas (Sindicato)'],
            ['branch_code' => '801-S', 'branch_name' => 'Planta Charcas (Sindicato)']
        ]);
    }
}
