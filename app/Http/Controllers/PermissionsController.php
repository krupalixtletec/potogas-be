<?php

namespace App\Http\Controllers;
use Request;
use Helper;
use App\branch;

class PermissionsController extends Controller
{
    public function index()
    {
        
    }
    public function getBranchs()
    {        
        $branches = Helper::getBranchs();
        return $branches;
    }
    public function getUsers()
    {
        return Helper::getUsers();
    }
    public function getRols()
    {
        return Helper::getRols();
    }
    public function deleteRol()
    {
        $input = Request::all(); 
        Helper::deleteRol($input['key']);
    }
    public function updateUser()
    {
        $input = Request::all(); 
        $values = json_decode($input['values']);
        $user_id = $input['key'];
        if(isset($values->branchs))
        {
            Helper::updateMappingBranchs($values->branchs, $user_id);
        }

        if(isset($values->rols))
        {
            Helper::updateMappingRols($values->rols, $user_id);
        }
        return;
    }
    public function updateRols()
    {
        $input = Request::all();
        Helper::updateRols($input['id'],$input['name'],$input['claims']);
        return;
    }
}
