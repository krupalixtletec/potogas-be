<?php

namespace App\Http\Controllers;

use App\salesPeriod;
use App\cylinderSalesDetails;
use Request;
use Response;
use Validator;
use Carbon\Carbon;
use Helper;
use App\branch;
use App\shifts;
use App\shiftBranchMapping;

class cylinderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
        ];
        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  200,
                'error' => $validator->messages()
            ],200);
        }

        $cylinderSalesDetails = array();
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $cylinderDetails = salesPeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'sales_period.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'sales_period.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','Ci')->first();

        if(empty($cylinderDetails)){
            return Response::json([
				'status' => 400,
				'error' => "Details not exist"
			],400);
        }
       
        $shiftData = shiftBranchMapping::find($cylinderDetails->id);
        $sales = $shiftData->cylinderSalesDetails->groupBy('ruta_code');
        $saleDetails = array();
        $count = 0;
        foreach($sales->toArray() as $saleKey => $saleValue){
            $innerCount = 0;
            $saleDetails[$count]['ruta'] = $saleValue[0]['ruta']; 
            $saleDetails[$count]['ruta_code'] = $saleKey; 
            $sortedSaleValue = collect($saleValue)->sortBy('sale_date')->all();
            foreach($sortedSaleValue as $key => $value){
                //$sale_val = (empty($value['sale_val'])) ? null : $value['sale_val'];
                $sale_val = $value['sale_val'];
                $saleDetails[$count]['value'][$innerCount] = array('sale_date'=>$this->displayDateFormat($value['sale_date']),'sale_val'=> $sale_val,'is_freezed'=> $value['is_freezed']);
                $innerCount++;
            }
            $count++;
        }

        $cylinderSalesDetails['branch_name'] = $cylinderDetails->branch_name;
        $cylinderSalesDetails['branch_code'] = $cylinderDetails->branch_code;
        $cylinderSalesDetails['shift_name'] = $cylinderDetails->shift;
        $cylinderSalesDetails['shift_code'] = $cylinderDetails->shift_code;        
        $cylinderSalesDetails['period_key'] = $cylinderDetails->period_key;
        $cylinderSalesDetails['is_period_freezed'] = $cylinderDetails->is_period_close;
        $cylinderSalesDetails['period_start_date'] = $this->displayDateFormat($cylinderDetails->period_start_date);
        $cylinderSalesDetails['period_end_date'] = $this->displayDateFormat($cylinderDetails->period_end_date);
        $cylinderSalesDetails['litros_sales_details'] = $saleDetails;

        return Response::json([
			'status' => 200,
			'results' => $cylinderSalesDetails
		],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        
        $input = Request::all();       
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
			'litros_sales_details' => 'required',
            'litros_sales_details.*.ruta_code' => 'required',
            'litros_sales_details.*.value.*.sale_date' => 'required|date|after_or_equal:period_start_date|before_or_equal:period_end_date',
            'litros_sales_details.*.value.*.sale_val' => 'required|numeric'
        ];
        $message = [
            'litros_sales_details.*.value.*.sale_date.required' => 'Sale date is required.',
            'litros_sales_details.*.value.*.sale_date.date' => 'Sale date is not a valid date.',
            'litros_sales_details.*.value.*.sale_date.after_or_equal' => 'Sale date is between Period start and end date.',
            'litros_sales_details.*.value.*.sale_date.before_or_equal' => 'Sale date is between Period start and end date.',
            'litros_sales_details.*.value.*.sale_val.required' => 'Sale Value is required.',
            'litros_sales_details.*.value.*.sale_val.numeric' => 'Sale Value must be a number.',
        ];

        $validator = Validator::make($input, $rules, $message);
 
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }
        
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $sales = salesPeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'sales_period.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'sales_period.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','Ci')->first();
        

        if(empty($sales)){
            return Response::json([
                'status' => 601,
                'error' => "Branch details dose not exist."
            ],400);
        }
      
        $errorResponse = array();
        if(!empty($input['litros_sales_details'])){
            foreach($input['litros_sales_details'] as $sKey => $details){
                $ruta_code = $details['ruta_code'];
                if(!empty($ruta_code) && is_array($details['value'])){
                    foreach($details['value'] as $value){
                      $is_freezed = isset($value['is_freezed']) ? $value['is_freezed'] : 0;
                      $sDetails =  cylinderSalesDetails::where('shifts_mapping_id', $sales->id)
                        ->where('sale_date', $this->changeDateFormat($value['sale_date']))
                        ->where('ruta_code', $ruta_code)->first();
                        if(empty($sDetails)){
                            $errorResponse[] = "$employee_id:{$value['att_date']} - Attendance Variable details not found.";
                        }else if(!$sDetails->is_freezed){
                            $isUpdate =  $sDetails->update(['sale_val' => $value['sale_val'], 'is_freezed' => $is_freezed ]);
                            if(!$isUpdate){
                                $errorResponse[] = "RUTA $ruta_code:{$value['sale_date']} - invalid sales value.";
                            }
                        }else{
                            $errorResponse[] = "RUTA $ruta_code:{$value['sale_date']} - sales value already updated.";
                        }
                    }
                }
            }
        }
        
        if($sales->id && empty($errorResponse)){
            return Response::json([
                'status' =>  200,
                'success' => "Cylinder data successfully Updated."
            ],200);
        }
        if(!empty($errorResponse)){
            return Response::json([
                'status' => 601,
				'error' => $errorResponse
			],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user_branch()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_id' => 'required']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  200,
                'error' => $validator->messages()
            ],200);
        }
        $branch = Helper::getUserBrach($input['user_id']);
        
        $branchIds = Helper::insertCylinderDetails($branch);
        //$branchData = salesPeriod::find($branchIds);

        $branchs = salesPeriod::join('branches', 'branches.id', '=', 'sales_period.branch_id' )
            ->whereIn('sales_period.id', $branchIds)
            ->where('type','Ci')
            ->orderBy('period_start_date','desc')
            ->get()->toArray();

        $cylinderSalesDetail = array();
        $isDetails = true;
        foreach((array)$branchs as $bKey => $branchDetails){
            $branchData = branch::find($branchDetails['id']);
            $shiftDetails = array();
            $shiftCount = 0;
            
            
            foreach($branchData->shifts as $lKey => $shiftData){
                $saleDetails = array(); 
                if(!empty($shiftData->cylinderSalesDetails->toArray())){

                    if($isDetails == true){
                        $sales = $shiftData->cylinderSalesDetails->groupBy('ruta_code'); 
                        $count = 0;
                        foreach($sales->toArray() as $saleKey => $saleValue){
                            $innerCount = 0;
                            $saleDetails[$count]['ruta'] = $saleValue[0]['ruta']; 
                            $saleDetails[$count]['ruta_code'] = $saleKey; 
                            $sortedSaleValue = collect($saleValue)->sortBy('sale_date')->all();
                            foreach($sortedSaleValue as $key => $value){
                               // $sale_val = (empty($value['sale_val'])) ? null : $value['sale_val'];
                                $sale_val = $value['sale_val'];
                                $saleDetails[$count]['value'][$innerCount] = array('sale_date'=>$this->displayDateFormat($value['sale_date']),'sale_val'=> $sale_val,'is_freezed'=> $value['is_freezed']);
                                $innerCount++;
                            }
                            $count++;
                        }
                        $isDetails = false;
                    }

                    $shiftDetails[$shiftCount]['shift_name'] = $shiftData->shift->shift;
                    $shiftDetails[$shiftCount]['shift_code'] = $shiftData->shift->shift_code;
                    if(!empty($saleDetails)){
                        $shiftDetails[$shiftCount]['litros_sales_details'] = $saleDetails;
                    }
                    $shiftCount++;
                }
            }
            if(!empty($shiftDetails)){
                $cylinderSalesDetail[$bKey]['period_start_date'] = $this->displayDateFormat($branchDetails['period_start_date']);
                $cylinderSalesDetail[$bKey]['period_end_date'] = $this->displayDateFormat($branchDetails['period_end_date']);
                $cylinderSalesDetail[$bKey]['branch_name'] = $branchData->branch_name;
                $cylinderSalesDetail[$bKey]['branch_code'] = $branchData->branch_code;
                $cylinderSalesDetail[$bKey]['period_key'] = $branchDetails['period_key'];
                $cylinderSalesDetail[$bKey]['is_period_freezed'] = $branchDetails['is_period_close'];
                $cylinderSalesDetail[$bKey]['shifts'] = $shiftDetails;
            }
        }
        
        return Response::json([
			'status' => 200,
			'results' => array_values($cylinderSalesDetail)
		],200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
