<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\branch;
use App\userBranchMapping;
use Validator;
use Helper;
use App\User;

class branchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = branch::all();
        
		$branchDetails = array();
		foreach($branch as $key => $data){
			$branchDetails[$key]['id'] = $data->id;
			$branchDetails[$key]['branch_code'] = $data->branch_code;
			$branchDetails[$key]['branch_name'] = $data->branch_name;
		}
		
		return Response::json([
			'status' => 200,
			'results' => $branchDetails
		],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userbranchlist()
    {
        $input = Request::all();     

        if(isset($input['user_email'])){
            $validator = Validator::make($input, ['user_email' => 'required|email']);
        }else{
            $validator = Validator::make($input, ['user_id' => 'required']);
        }

        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        if(!empty($input['user_email'])){
            $userData = Helper::UserIdByEmail($input['user_email']);
        }else{
            $userData = User::where('user_id',$input['user_id'])->first();
        }
        if(empty($userData)){
            return Response::json([
                'status' =>  601,
                'error' => "User details not exists."
            ],200);
        }

        $userBranch = userBranchMapping::where('user_id',$userData->id)->get();

        if(empty($userBranch->toArray())){
            return Response::json([
                'status' =>  601,
                'error' => "Branch details not exists."
            ],200);
        }
        $branchDetails = array();
		foreach($userBranch as $key => $data){
			$branchDetails[$key]['id'] = $data->branch->id;
			$branchDetails[$key]['branch_code'] = $data->branch->branch_code;
			$branchDetails[$key]['branch_name'] = $data->branch->branch_name;
        }
        
        return Response::json([
            'status' =>  200,
            'results' => $branchDetails
        ],200);
    }

    
}
