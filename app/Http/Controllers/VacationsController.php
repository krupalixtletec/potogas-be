<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Vacation;
use Validator;
use Carbon\Carbon;
use Helper;

class VacationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_email' => 'required|email']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $userData = Helper::UserIdByEmail($input['user_email']);
        if(empty($userData->user_id)){
            return Response::json([
                'status' =>  601,
                'error' => "User not exist please check email address."
            ],200);
        }else{
            $roleData = Helper::UserRoleById($userData->user_id);

            if(!empty($roleData->role_code) && $roleData->role_code == 'admin'){
                $vacationResponse = Vacation::all();
            }else{
                $vacationResponse = Vacation::where('user_id',$userData->user_id)->get();
            }

            return Response::json([
                'status' => 200,
                'results' => $vacationResponse
            ],200);
        }

        
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'user_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'employee_name' => 'required',
            'start_date' => 'required|date',
            'duration' => 'numeric',
            'payment_date' => 'date',
            'discanso_1' => 'integer|between:1,7|different:discanso_2|nullable',
            'discanso_2' => 'integer|between:1,7|different:discanso_1|nullable',
        ];

        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $array['user_id'] = $input['user_id'];
        $array['employee_id'] = $input['employee_id'];
        $array['employee_name'] = $input['employee_name'];
        $array['start_date'] = $this->changeDateFormat($input['start_date']);
        $array['duration'] = $input['duration'];
        $array['payment_date'] = $this->changeDateFormat($input['payment_date']);
        $array['discanso_1'] = $input['discanso_1'];
        $array['discanso_2'] = $input['discanso_2'];

        
        if(empty($input['id'])){
            $details = Vacation::firstOrCreate($array);
        }else{
            $isExist = Vacation::find($input['id']);            
            if(!empty($isExist)){
                $details = Vacation::updateOrCreate(['id' => $input['id']],$array);
            }else{
                return Response::json([
                    'status' =>  601,
                    'error' => "Vacation details not exists."
                ],200);
            }
        }

        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $details = Vacation::find($id);

        if(empty($details->id)){
            return Response::json([
                'status' =>  601,
                'error' => "Vacation details not exists."
            ],200);
        }
        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    
    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
