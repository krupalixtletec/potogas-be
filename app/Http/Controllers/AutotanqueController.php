<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Autotanque;
use Validator;
use Carbon\Carbon;
use Helper;

class AutotanqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_email' => 'required|email']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $responseData = array();

        $userData = Helper::UserIdByEmail($input['user_email']);
        if(empty($userData->user_id)){
            return Response::json([
                'status' =>  601,
                'error' => "User not exist please check email address."
            ],200);
        }else{
            $roleData = Helper::UserRoleById($userData->user_id);

            if(!empty($roleData->role_code) && $roleData->role_code == 'admin'){
                $autotanqueData = Autotanque::where('parent_ID',null)->get();
                if(!empty($autotanqueData)){
                    foreach($autotanqueData as $details){
                        $responseData[] = $this->displayResponseFormat($details,$details->helperDetail);
                    }
                }
            }else{
                $autotanqueData = Autotanque::where('user_id',$userData->user_id)->where('parent_ID',null)->get();
                if(!empty($autotanqueData)){
                    foreach($autotanqueData as $details){
                        $responseData[] = $this->displayResponseFormat($details,$details->helperDetail);
                    }
                }
            }

            return Response::json([
                'status' => 200,
                'results' => $responseData
            ],200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'user_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'employee_name' => 'required',
            'date' => 'required|date',
            'liters' => 'numeric',
            'services' => 'numeric',
            'exported_to_processo' => 'max:1'
        ];

        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $helper = "";

        $array['user_id'] = $input['user_id'];
        $array['employee_id'] = $input['employee_id'];
        $array['employee_name'] = $input['employee_name'];
        $array['date'] = $this->changeDateFormat($input['date']);
        $array['route_code'] = $input['route_code'];
        $array['branch_code'] = $input['branch_code'];
        $array['truck_number'] = $input['truck_number'];
        $array['destajo_type'] = $input['destajo_type'];
        $array['liters'] = $input['liters'];
        $array['service_activity_code'] = $input['service_activity_code'];
        $array['services'] = $input['services'];
        $array['exported_to_processo'] = $input['exported_to_processo'];
        
        $employeeArray = $array;
        $employeeArray['activity_code'] = $input['activity_code'];

        

        if(empty($input['id'])){
            
            $emp = Autotanque::firstOrCreate($employeeArray);
            if(!empty($emp->id)){
                if(!empty($input['helper_activity_code'])){
                    $helperArray = $array;
                    $helperArray['activity_code'] = $input['helper_activity_code'];
                    $helperArray['parent_ID'] = $emp->id;

                    $helper = Autotanque::firstOrCreate($helperArray);
                }
            }
        }else{            
            $isExist = Autotanque::find($input['id']);            
            if(!empty($isExist) && $isExist->parent_ID == null){
                $emp = Autotanque::updateOrCreate(['id' => $input['id']],$employeeArray);
                if(!empty($emp->id)){
                    if(!empty($input['helper_activity_code'])){
                        $helperArray = $array;
                        $helperArray['activity_code'] = $input['helper_activity_code'];
                        $helperArray['parent_ID'] = $emp->id;
    
                        $helper = Autotanque::updateOrCreate(['parent_ID' => $emp->id],$helperArray);
                    }
                }
            }else{
                return Response::json([
                    'status' =>  601,
                    'error' => "Autotanque details not exists."
                ],200);
            }
        }

        $responseData = $this->displayResponseFormat($emp,$helper);
 
        return Response::json([
			'status' => 200,
			'results' => $responseData
		],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $details = Autotanque::find($id);

        if(empty($details->id)){
            return Response::json([
                'status' =>  601,
                'error' => "Incapacidade details not exists."
            ],200);
        }

        $responseData = $this->displayResponseFormat($details,$details->helperDetail);

        return Response::json([
			'status' => 200,
			'results' => $responseData
		],200);
    }

    public function displayResponseFormat($empDetails,$helperDetails){
        $responseData = array();

        // Employee Details
        $responseData['employee_details']['id'] = $empDetails->id;
        $responseData['employee_details']['user_id'] = $empDetails->user_id;
        $responseData['employee_details']['employee_id'] = $empDetails->employee_id;
        $responseData['employee_details']['employee_name'] = $empDetails->employee_name;
        $responseData['employee_details']['date'] = $this->displayDateFormat($empDetails->date);
        $responseData['employee_details']['route_code'] = $empDetails->route_code;
        $responseData['employee_details']['activity_code'] = $empDetails->activity_code;
        $responseData['employee_details']['branch_code'] = $empDetails->branch_code;
        $responseData['employee_details']['truck_number'] = $empDetails->truck_number;
        $responseData['employee_details']['destajo_type'] = $empDetails->destajo_type;
        $responseData['employee_details']['liters'] = $empDetails->liters;
        $responseData['employee_details']['service_activity_code'] = $empDetails->service_activity_code;
        $responseData['employee_details']['services'] = $empDetails->services;
        $responseData['employee_details']['exported_to_processo'] = $empDetails->exported_to_processo;
        $responseData['employee_details']['created_by'] = $empDetails->created_by;
        $responseData['employee_details']['updated_by'] = $empDetails->updated_by;
        $responseData['employee_details']['created_at'] = $empDetails->created_at;
        $responseData['employee_details']['updated_at'] = $empDetails->updated_at;

        // Helper Details
        if(!empty($helperDetails)){
            $responseData['helper_details']['id'] = $helperDetails->id;
            $responseData['helper_details']['user_id'] = $helperDetails->user_id;
            $responseData['helper_details']['employee_id'] = $helperDetails->employee_id;
            $responseData['helper_details']['employee_name'] = $helperDetails->employee_name;
            $responseData['helper_details']['date'] = $this->displayDateFormat($helperDetails->date);
            $responseData['helper_details']['route_code'] = $helperDetails->route_code;
            $responseData['helper_details']['activity_code'] = $helperDetails->activity_code;
            $responseData['helper_details']['branch_code'] = $helperDetails->branch_code;
            $responseData['helper_details']['truck_number'] = $helperDetails->truck_number;
            $responseData['helper_details']['destajo_type'] = $helperDetails->destajo_type;
            $responseData['helper_details']['liters'] = $helperDetails->liters;
            $responseData['helper_details']['service_activity_code'] = $helperDetails->service_activity_code;
            $responseData['helper_details']['services'] = $helperDetails->services;
            $responseData['helper_details']['exported_to_processo'] = $helperDetails->exported_to_processo;
            $responseData['helper_details']['created_by'] = $helperDetails->created_by;
            $responseData['helper_details']['updated_by'] = $helperDetails->updated_by;
            $responseData['helper_details']['created_at'] = $helperDetails->created_at;
            $responseData['helper_details']['updated_at'] = $helperDetails->updated_at;
        }
        return $responseData;
    }

    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
