<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Incapacidades;
use Validator;
use Carbon\Carbon;
use Helper;

class IncapacidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_email' => 'required|email']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $userData = Helper::UserIdByEmail($input['user_email']);
        if(empty($userData->user_id)){
            return Response::json([
                'status' =>  601,
                'error' => "User not exist please check email address."
            ],200);
        }else{
            $roleData = Helper::UserRoleById($userData->user_id);

            if(!empty($roleData->role_code) && $roleData->role_code == 'admin'){
                $vacationResponse = Incapacidades::all();
            }else{
                $vacationResponse = Incapacidades::where('user_id',$userData->user_id)->get();
            }

            return Response::json([
                'status' => 200,
                'results' => $vacationResponse
            ],200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'user_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'employee_name' => 'required',
            'numero_de_folio' => 'required',
            'start_date' => 'required|date',
            'discount_date' => 'required|date',
            'duration' => 'required|integer',
            'date_of_accident' => 'date',
            'day' => 'integer|between:1,7',
            'time' => 'date_format:H:i'
        ];

        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $array['user_id'] = $input['user_id'];
        $array['employee_id'] = $input['employee_id'];
        $array['employee_name'] = $input['employee_name'];
        $array['numero_de_folio'] = $input['numero_de_folio'];
        $array['start_date'] = $this->changeDateFormat($input['start_date']);
        $array['discount_date'] = $this->changeDateFormat($input['discount_date']);
        $array['duration'] = $input['duration'];
        $array['rama'] = $input['rama'];
        $array['risk_type'] = $input['risk_type'];
        $array['consecuence'] = $input['consecuence'];
        $array['control'] = $input['control'];
        $array['date_of_accident'] = $this->changeDateFormat($input['date_of_accident']);
        $array['place_of_accident'] = $input['place_of_accident'];
        $array['accident_cause'] = $input['accident_cause'];
        $array['type_of_injury'] = $input['type_of_injury'];
        $array['corrective_measure'] = $input['corrective_measure'];
        $array['day'] = $input['day'];
        $array['time'] = $input['time'];
        
        if(empty($input['id'])){
            $details = Incapacidades::firstOrCreate($array);
        }else{
            $isExist = Incapacidades::find($input['id']);            
            if(!empty($isExist)){
                $details = Incapacidades::updateOrCreate(['id' => $input['id']],$array);
            }else{
                return Response::json([
                    'status' =>  601,
                    'error' => "Incapacidades details not exists."
                ],200);
            }
        }

        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $details = Incapacidades::find($id);

        if(empty($details->id)){
            return Response::json([
                'status' =>  601,
                'error' => "Incapacidade details not exists."
            ],200);
        }
        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
