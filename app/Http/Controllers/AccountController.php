<?php

namespace App\Http\Controllers;
use Google_Client; 
use Request;
use Helper;

class AccountController extends Controller
{
    public function index()
    {
        
    }
    public function login()
    {
        $input = Request::all();
        $email = $input['email'];
        $password = $input['password'];
        //Helper::insertUser($email);
        $result['id'] = Helper::loginByLocal($email,$password);
        if(!$result['id'])
        {
            $result['error'] = 'No existe el usuario en la bd o Usuario/Contrasenia no coincide';
        }
        else{
            $result['claims'] = Helper::getClaimsByEmail($email); 
            $result['Email']=$email;
            $result['Name']=$email;
            $result['LastName']=$email;
            $result['Claims']=Helper::getClaimsByEmail($email);
            $result['Id'] = Helper::getIdByEmail($email);
        }
       
        return $result;
    }
    public function verifyToken()
    {        
        $input = Request::all();
        $client = new Google_Client([
            //'client_id' => '871841198873-1dajfn2v2896tfcklsaahnqjir5vj8af.apps.googleusercontent.com'
            'client_id' => env('API_GOOGLE_CODE1')
        ]);
        //$body = json_decode(file_get_contents("php://input"), true);
        $idToken = $input['token'];
        try {
            $payload = $client->verifyIdToken($idToken);
            if(!$payload)
            {
                $client2 = new Google_Client([
                'client_id' => env('API_GOOGLE_CODE2')
                ]);
                
                $payload = $client2->verifyIdToken($idToken);
            }
            //Helper::setUser($payload);
            $payload['id'] = Helper::getIdByEmail($payload['email']);
            if(!$payload['id'])
            {
                $payload['error'] = 'Este usuario no tiene permiso de acceder a esta aplicacion';
            }
            else{
                $payload['claims'] = Helper::getClaimsByEmail($payload['email']);
            }
        } catch (Exception $e) {
            echo 'exception    ';
            echo $e;
        }
        if($payload)
        {
            return json_encode($payload);
        }
        else 
        {
            return null;
        }
    }
}
