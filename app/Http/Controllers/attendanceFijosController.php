<?php

namespace App\Http\Controllers;

use Request;
use Response;
use Validator;
use Carbon\Carbon;
use Helper;
use App\branch;
use App\shifts;
use App\attendancePeriod;
use App\attendanceFijos;
use App\shiftBranchMapping;

class attendanceFijosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
        ];
        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  200,
                'error' => $validator->messages()
            ],200);
        }

        $cylinderSalesDetails = array();
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $attDetails = attendancePeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'attendance_periods.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'attendance_periods.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','F')->first();

        if(empty($attDetails)){
            return Response::json([
				'status' => 400,
				'error' => "Details not exist"
			],400);
        }
       
        $shiftData = shiftBranchMapping::find($attDetails->id);
        $sales = $shiftData->attendanceFijos->groupBy('employee_id'); 
        $attSalesDetail = array();
        $saleDetails = array();
        $count = 0;
        foreach($sales->toArray() as $saleKey => $saleValue){
            $innerCount = 0;
            $saleDetails[$count]['employee_name'] = $saleValue[0]['employee_name']; 
            $saleDetails[$count]['employee_code'] = $saleValue[0]['employee_code']; 
            $saleDetails[$count]['employee_id'] = $saleKey; 
            $saleDetails[$count]['puesto'] = $saleValue[0]['puesto'];
            $sortedSaleValue = collect($saleValue)->sortBy('sale_date')->all();
            foreach($sortedSaleValue as $key => $value){
                //$tiempo_extra = (empty($value['tiempo_extra'])) ? null : $value['tiempo_extra'];
                $tiempo_extra = $value['tiempo_extra'];
                $saleDetails[$count]['value'][$innerCount] = array(
                    'att_date'=>$this->displayDateFormat($value['att_date']),
                    'incidencia_code'=> $value['incidencia_code'],
                    'tiempo_extra'=> $tiempo_extra,
                    'viaticos'=> $value['viaticos'],
                    'is_freezed'=> $value['is_freezed']
                );
                $innerCount++;
            }
            $count++;
        }

        $attSalesDetail['branch_name'] = $attDetails->branch_name;
        $attSalesDetail['branch_code'] = $attDetails->branch_code;
        $attSalesDetail['shift_name'] = $attDetails->shift;
        $attSalesDetail['shift_code'] = $attDetails->shift_code;        
        $attSalesDetail['period_key'] = $attDetails->period_key;
        $attSalesDetail['is_period_freezed'] = $attDetails->is_period_close;
        $attSalesDetail['period_start_date'] = $this->displayDateFormat($attDetails->period_start_date);
        $attSalesDetail['period_end_date'] = $this->displayDateFormat($attDetails->period_end_date);
        $attSalesDetail['attendance_fijos_details'] = $saleDetails;

        return Response::json([
			'status' => 200,
			'results' => $attSalesDetail
		],200);
    }

    // Update 
    public function insert()
    {
        
        $input = Request::all();       
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
			'attendance_fijos_details' => 'required',
            'attendance_fijos_details.*.employee_id' => 'required',
            'attendance_fijos_details.*.value.*.att_date' => 'required|date|after_or_equal:period_start_date|before_or_equal:period_end_date',
            /* 'attendance_fijos_details.*.value.*.incidencia_code' => 'required', */
            'attendance_fijos_details.*.value.*.tiempo_extra' => 'numeric',
            /* 'attendance_fijos_details.*.value.*.viaticos' => 'required' */
        ];
        $message = [
            'attendance_fijos_details.*.value.*.att_date.required' => 'Attendance date is required.',
            'attendance_fijos_details.*.value.*.att_date.date' => 'Attendance date is not a valid date.',
            'attendance_fijos_details.*.value.*.att_date.after_or_equal' => 'Attendance date is between Period start and end date.',
            'attendance_fijos_details.*.value.*.att_date.before_or_equal' => 'Attendance date is between Period start and end date.',
            'attendance_fijos_details.*.value.*.incidencia_code' => 'Attendance incidencia code is required.',
            'attendance_fijos_details.*.value.*.tiempo_extra.required' => 'Attendance tiempo extra is required.',
            'attendance_fijos_details.*.value.*.tiempo_extra.numeric' => 'Attendance tiempo extra must be a number..',
            'attendance_fijos_details.*.value.*.viaticos' => 'Attendance viaticos is required.'
        ];

        $validator = Validator::make($input, $rules, $message);
 
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }
        
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $sales = attendancePeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'attendance_periods.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'attendance_periods.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','F')->first();


        if(empty($sales)){
            return Response::json([
                'status' => 601,
                'error' => "details dose not exist."
            ],400);
        }
      
        $errorResponse = array();
        if(!empty($input['attendance_fijos_details'])){
            foreach($input['attendance_fijos_details'] as $sKey => $details){
                $employee_id = $details['employee_id'];
                if(!empty($employee_id) && is_array($details['value'])){
                    foreach($details['value'] as $value){
                      $is_freezed = isset($value['is_freezed']) ? $value['is_freezed'] : 0;
                      $sDetails =  attendanceFijos::where('shifts_mapping_id', $sales->id)
                        ->where('att_date', $this->changeDateFormat($value['att_date']))
                        ->where('employee_id', $employee_id)->first();
                        if(empty($sDetails)){
                            $errorResponse[] = "$employee_id:{$value['att_date']} - Attendance Variable details not found.";
                        }else if(!$sDetails->is_freezed){
                            $isUpdate = $sDetails->update(['incidencia_code' => $value['incidencia_code'], 'tiempo_extra' => $value['tiempo_extra'], 'viaticos' => $value['viaticos'], 'is_freezed' => $is_freezed ]);
                            if(!$isUpdate){
                                $errorResponse[] = "$employee_id:{$value['att_date']} - invalid Fijos value.";
                            }
                        }else{
                            $errorResponse[] = "$employee_id:{$value['att_date']} - Fijos value already updated.";
                        }
                    }
                }
            }
        }
        
        if($sales->id && empty($errorResponse)){
            return Response::json([
                'status' =>  200,
                'success' => "Attendance Fijos data successfully Updated."
            ],200);
        }
        if(!empty($errorResponse)){
            return Response::json([
                'status' => 601,
				'error' => $errorResponse
			],400);
        }
    }
    
    public function user_branch()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_id' => 'required']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  200,
                'error' => $validator->messages()
            ],200);
        }
        $branch = Helper::getUserBrach($input['user_id']);

        $branchIds = Helper::insertAttendanceFijosDetails($branch);

        
        $branchs = attendancePeriod::join('branches', 'branches.id', '=', 'attendance_periods.branch_id' )
            ->whereIn('attendance_periods.id', $branchIds)
            ->where('type','F')
            ->orderBy('period_start_date','desc')
            ->get()->toArray();

        $attSalesDetail = array();
        $isDetails = true;

        foreach((array)$branchs as $bKey => $branchDetails){
            $branchData = branch::find($branchDetails['id']);
            $shiftDetails = array();
            $shiftCount = 0;
            
            
            foreach($branchData->shifts as $lKey => $shiftData){
                $saleDetails = array(); 
                if(!empty($shiftData->attendanceFijos->toArray())){

                    if($isDetails == true){
                        $sales = $shiftData->attendanceFijos->groupBy('employee_id');
                        $count = 0;
                        foreach($sales->toArray() as $saleKey => $saleValue){
                            $innerCount = 0;
                            $saleDetails[$count]['employee_name'] = $saleValue[0]['employee_name']; 
                            $saleDetails[$count]['employee_code'] = $saleValue[0]['employee_code']; 
                            $saleDetails[$count]['employee_id'] = $saleKey; 
                            $saleDetails[$count]['puesto'] = $saleValue[0]['puesto']; 
                            $sortedSaleValue = collect($saleValue)->sortBy('att_date')->all();
                            foreach($sortedSaleValue as $key => $value){
                                //$tiempo_extra = (empty($value['tiempo_extra'])) ? null : $value['tiempo_extra'];
                                $tiempo_extra = $value['tiempo_extra'];
                                $saleDetails[$count]['value'][$innerCount] = array(
                                                    'att_date'=>$this->displayDateFormat($value['att_date']),
                                                    'incidencia_code'=> $value['incidencia_code'],
                                                    'tiempo_extra'=> $tiempo_extra,
                                                    'viaticos'=> $value['viaticos'],
                                                    'is_freezed'=> $value['is_freezed']
                                                );
                                $innerCount++;
                            }
                            $count++;
                        }
                        $isDetails = false;
                    }

                    $shiftDetails[$shiftCount]['shift_name'] = $shiftData->shift->shift;
                    $shiftDetails[$shiftCount]['shift_code'] = $shiftData->shift->shift_code;
                    if(!empty($saleDetails)){
                        $shiftDetails[$shiftCount]['attendance_fijos_details'] = $saleDetails;
                    }
                    $shiftCount++;
                }
            }
            if(!empty($shiftDetails)){
                $attSalesDetail[$bKey]['period_start_date'] = $this->displayDateFormat($branchDetails['period_start_date']);
                $attSalesDetail[$bKey]['period_end_date'] = $this->displayDateFormat($branchDetails['period_end_date']);
                $attSalesDetail[$bKey]['branch_name'] = $branchData->branch_name;
                $attSalesDetail[$bKey]['branch_code'] = $branchData->branch_code;
                $attSalesDetail[$bKey]['period_key'] = $branchDetails['period_key'];
                $attSalesDetail[$bKey]['is_period_freezed'] = $branchDetails['is_period_close'];
                $attSalesDetail[$bKey]['shifts'] = $shiftDetails;
            }
        }
      
        return Response::json([
			'status' => 200,
			'results' => array_values($attSalesDetail)
		],200);

    }

    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
   
}
