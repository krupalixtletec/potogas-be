<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Ausentismo;
use Validator;
use Carbon\Carbon;
use Helper;

class AusentismoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_email' => 'required|email']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $userData = Helper::UserIdByEmail($input['user_email']);
        if(empty($userData->user_id)){
            return Response::json([
                'status' =>  601,
                'error' => "User not exist please check email address."
            ],200);
        }else{
            $roleData = Helper::UserRoleById($userData->user_id);

            if(!empty($roleData->role_code) && $roleData->role_code == 'admin'){
                $vacationResponse = Ausentismo::all();
            }else{
                $vacationResponse = Ausentismo::where('user_id',$userData->user_id)->get();
            }

            return Response::json([
                'status' => 200,
                'results' => $vacationResponse
            ],200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'user_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'employee_name' => 'required',
            'start_date' => 'required|date',
            'ausentismo_type' => 'required',
            'hours' => 'required|integer',
            'minutes' => 'required|integer'
        ];

        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $array['user_id'] = $input['user_id'];
        $array['employee_id'] = $input['employee_id'];
        $array['employee_name'] = $input['employee_name'];
        $array['start_date'] = $this->changeDateFormat($input['start_date']);
        $array['ausentismo_type'] = $input['ausentismo_type'];
        $array['hours'] = $input['hours'];
        $array['minutes'] = $input['minutes'];
        $array['comments'] = $input['comments'];
        
        if(empty($input['id'])){
            $details = Ausentismo::firstOrCreate($array);
        }else{
            $isExist = Ausentismo::find($input['id']);            
            if(!empty($isExist)){
                $details = Ausentismo::updateOrCreate(['id' => $input['id']],$array);
            }else{
                return Response::json([
                    'status' =>  601,
                    'error' => "Ausentismo details not exists."
                ],200);
            }
        }

        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $details = Ausentismo::find($id);

        if(empty($details->id)){
            return Response::json([
                'status' =>  601,
                'error' => "Ausentismo details not exists."
            ],200);
        }
        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    
    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
