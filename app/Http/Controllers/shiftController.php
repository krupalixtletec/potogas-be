<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\shifts;

class shiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shifts = shifts::all();
        
		$shiftDetails = array();
		foreach($shifts as $key => $data){
			$shiftDetails[$key]['id'] = $data->id;
			$shiftDetails[$key]['shift_code'] = $data->shift_code;
			$shiftDetails[$key]['shift'] = $data->shift;
		}
		
		return Response::json([
			'status' => 200,
			'results' => $shiftDetails
		],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
