<?php

namespace App\Http\Controllers;

use App\salesPeriod;
use App\servicosSalesDetails;
use Request;
use Response;
use Validator;
use Carbon\Carbon;
use Helper;
use App\branch;
use App\shifts;
use App\shiftBranchMapping;

class servicosDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
        ];
        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }
        

        $servicosSalesDetails = array();
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $litrosDetails =  salesPeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'sales_period.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'sales_period.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','Se')->first();
        
        
        if(empty($litrosDetails)){
            return Response::json([
				'status' => 601,
				'error' => "Details not exist"
			],400);
        }
       
       $shiftData = shiftBranchMapping::find($litrosDetails->id);
       $sales = $shiftData->servicosSalesDetails->groupBy('ruta_code');
       $saleDetails = array();
       $count = 0;
        foreach($sales->toArray() as $saleKey => $saleValue){
            $innerCount = 0;
            $saleDetails[$count]['ruta'] = $saleValue[0]['ruta']; 
            $saleDetails[$count]['ruta_code'] = $saleKey; 
            $sortedSaleValue = collect($saleValue)->sortBy('sale_date')->all();
            foreach($sortedSaleValue as $key => $value){
                //$sale_val = (empty($value['sale_val'])) ? null : $value['sale_val'];
                $sale_val = $value['sale_val'];
                $saleDetails[$count]['value'][$innerCount] = array('sale_date'=>$this->displayDateFormat($value['sale_date']),'sale_val'=> $sale_val,'is_freezed'=> $value['is_freezed']);
                $innerCount++;
            }
            $count++;
        }
        
        $servicosSalesDetails['branch_name'] = $litrosDetails->branch_name;
        $servicosSalesDetails['branch_code'] = $litrosDetails->branch_code;
        $servicosSalesDetails['shift_name'] = $litrosDetails->shift;
        $servicosSalesDetails['shift_code'] = $litrosDetails->shift_code;        
        $servicosSalesDetails['period_key'] = $litrosDetails->period_key;
        $servicosSalesDetails['is_period_freezed'] = $litrosDetails->is_period_close;
        $servicosSalesDetails['period_start_date'] = $this->displayDateFormat($litrosDetails->period_start_date);
        $servicosSalesDetails['period_end_date'] = $this->displayDateFormat($litrosDetails->period_end_date);
        $servicosSalesDetails['litros_sales_details'] = $saleDetails;

        return Response::json([
			'status' => 200,
			'results' => $servicosSalesDetails
		],200);
    
    }

    /**
     * insert the specified resource in storage.
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'period_start_date' => 'required|date',
            'period_end_date' => 'required|date',
            'branch_code' => 'required',
            'shift_code' => 'required',
            'litros_sales_details' => 'required',
            'litros_sales_details.*.ruta_code' => 'required',
            'litros_sales_details.*.value.*.sale_date' => 'required|date|after_or_equal:period_start_date|before_or_equal:period_end_date',
            'litros_sales_details.*.value.*.sale_val' => 'required|numeric'
        ];
        $message = [
            'litros_sales_details.*.value.*.sale_date.required' => 'Sale date is required.',
            'litros_sales_details.*.value.*.sale_date.date' => 'Sale date is not a valid date.',
            'litros_sales_details.*.value.*.sale_date.after_or_equal' => 'Sale date is between Period start and end date.',
            'litros_sales_details.*.value.*.sale_date.before_or_equal' => 'Sale date is between Period start and end date.',
            'litros_sales_details.*.value.*.sale_val.required' => 'Sale Value is required.',
            'litros_sales_details.*.value.*.sale_val.numeric' => 'Sale Value must be a number.',
        ];

        $validator = Validator::make($input, $rules, $message);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }
        
        $startDate = $this->changeDateFormat($input['period_start_date']);
        $endDate = $this->changeDateFormat($input['period_end_date']);
        $branch_code = $input['branch_code'];
        $shift_code = $input['shift_code'];
        
        $sales =  salesPeriod::select('*','shift_branch_mappings.id')->join('branches', 'branches.id', '=', 'sales_period.branch_id' )
            ->join('shift_branch_mappings', 'shift_branch_mappings.branch_id', '=', 'sales_period.branch_id' )
            ->join('shifts', 'shifts.id', '=', 'shift_branch_mappings.shifts_id' )
            ->where('period_start_date', $startDate)
            ->where('period_end_date', $endDate)
            ->where('branch_code',$branch_code)
            ->where('shift_code', $shift_code)
            ->where('type','Se')->first();

        if(empty($sales)){
            return Response::json([
				'status' => 601,
				'error' => "Branch details dose not exist."
			],400);
        }

        $errorResponse = array();
        if(!empty($input['litros_sales_details'])){
            foreach($input['litros_sales_details'] as $sKey => $details){
                $ruta_code = $details['ruta_code'];
                if(!empty($ruta_code) && is_array($details['value'])){
                    foreach($details['value'] as $value){
                       $is_freezed = isset($value['is_freezed']) ? $value['is_freezed'] : 0;
                      $sDetails =  servicosSalesDetails::where('shifts_mapping_id', $sales->id)
                        ->where('sale_date', $this->changeDateFormat($value['sale_date']))
                        ->where('ruta_code', $ruta_code)->first();
                        if(empty($sDetails)){
                            $errorResponse[] = "$employee_id:{$value['att_date']} - Attendance Variable details not found.";
                        }else if(!$sDetails->is_freezed){
                            $isUpdate = $sDetails->update(['sale_val' => $value['sale_val'], 'is_freezed' => $is_freezed ]);
                            if(!$isUpdate){
                                $errorResponse[] = "RUTA $ruta_code:{$value['sale_date']} - invalid sales value.";
                            }
                        }else{
                            $errorResponse[] = "RUTA $ruta_code:{$value['sale_date']} - sales value already updated.";
                        }
                    }
                }
            }
        }
        if($sales->id && empty($errorResponse)){
            return Response::json([
                'status' =>  200,
                'success' => "Servicos data successfully Updated."
            ],200);
        }
        if(!empty($errorResponse)){
            return Response::json([
                'status' => 601,
				'error' => $errorResponse
			],400);
        }
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user_branch()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_id' => 'required']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }
        $branch = Helper::getUserBrach($input['user_id']);
        
        $branchIds = Helper::insertServicosDetails($branch);
       
        $branchs = salesPeriod::join('branches', 'branches.id', '=', 'sales_period.branch_id' )
        ->whereIn('sales_period.id', $branchIds)          
        ->where('type','Se')
        ->orderBy('period_start_date','desc')
        ->get()->toArray();

        $litrosSalesDetail = array();
        $isDetails = true;
        foreach((array)$branchs as $bKey => $branchDetails){
            $branchData = branch::find($branchDetails['id']);
            $shiftDetails = array();
            $shiftCount = 0;
            
            
            foreach($branchData->shifts as $lKey => $shiftData){
                $saleDetails = array(); 
                if(!empty($shiftData->servicosSalesDetails->toArray())){

                    if($isDetails == true){
                        $sales = $shiftData->servicosSalesDetails->groupBy('ruta_code'); 
                        $count = 0;
                        foreach($sales->toArray() as $saleKey => $saleValue){
                            $innerCount = 0;
                            $saleDetails[$count]['ruta'] = $saleValue[0]['ruta']; 
                            $saleDetails[$count]['ruta_code'] = $saleKey; 
                            $sortedSaleValue = collect($saleValue)->sortBy('sale_date')->all();
                            foreach($sortedSaleValue as $key => $value){
                                //$sale_val = (empty($value['sale_val'])) ? null : $value['sale_val'];
                                $sale_val = $value['sale_val'];
                                $saleDetails[$count]['value'][$innerCount] = array('sale_date'=>$this->displayDateFormat($value['sale_date']),'sale_val'=> $sale_val,'is_freezed'=> $value['is_freezed']);
                                $innerCount++;
                            }
                            $count++;
                        }
                        $isDetails = false;
                    }

                    $shiftDetails[$shiftCount]['shift_name'] = $shiftData->shift->shift;
                    $shiftDetails[$shiftCount]['shift_code'] = $shiftData->shift->shift_code;
                    if(!empty($saleDetails)){
                        $shiftDetails[$shiftCount]['litros_sales_details'] = $saleDetails;
                    }
                    $shiftCount++;
                }
            }
            if(!empty($shiftDetails)){
                $litrosSalesDetail[$bKey]['period_start_date'] = $this->displayDateFormat($branchDetails['period_start_date']);
                $litrosSalesDetail[$bKey]['period_end_date'] = $this->displayDateFormat($branchDetails['period_end_date']);
                $litrosSalesDetail[$bKey]['branch_name'] = $branchData->branch_name;
                $litrosSalesDetail[$bKey]['branch_code'] = $branchData->branch_code;
                $litrosSalesDetail[$bKey]['period_key'] = $branchDetails['period_key'];
                $litrosSalesDetail[$bKey]['is_period_freezed'] = $branchDetails['is_period_close'];
                $litrosSalesDetail[$bKey]['shifts'] = $shiftDetails;
            }
        }

        return Response::json([
			'status' => 200,
			'results' => array_values($litrosSalesDetail)
		],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }

}
