<?php

namespace App\Http\Controllers;
use Request;
use Response;
use App\Cilindros;
use Validator;
use Carbon\Carbon;
use Helper;

class CilindrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Request::all();     
        $validator = Validator::make($input, ['user_email' => 'required|email']);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $userData = Helper::UserIdByEmail($input['user_email']);
        if(empty($userData->user_id)){
            return Response::json([
                'status' =>  601,
                'error' => "User not exist please check email address."
            ],200);
        }else{
            $roleData = Helper::UserRoleById($userData->user_id);

            if(!empty($roleData->role_code) && $roleData->role_code == 'admin'){
                $vacationResponse = Cilindros::all();
            }else{
                $vacationResponse = Cilindros::where('user_id',$userData->user_id)->get();
            }

            return Response::json([
                'status' => 200,
                'results' => $vacationResponse
            ],200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert()
    {
        $input = Request::all();      
        $rules =  [
            'user_id' => 'required|numeric',
            'employee_id' => 'required|numeric',
            'employee_name' => 'required',
            'date' => 'required|date',
            'capture_kilos' => 'integer',
            'exported_to_processo' => 'max:1'
        ];

        $validator = Validator::make($input, $rules);
                
        if ($validator->fails()) {
            return Response::json([
                'status' =>  601,
                'error' => $validator->messages()
            ],200);
        }

        $array['user_id'] = $input['user_id'];
        $array['employee_id'] = $input['employee_id'];
        $array['employee_name'] = $input['employee_name'];
        $array['date'] = $this->changeDateFormat($input['date']);
        $array['route_code'] = $input['route_code'];
        $array['activity_code'] = $input['activity_code'];
        $array['branch_code'] = $input['branch_code'];
        $array['truck_number'] = $input['truck_number'];
        $array['destajo_type'] = $input['destajo_type'];
        $array['capture_kilos'] = $input['capture_kilos'];
        $array['number_of_cilindros'] = $input['number_of_cilindros'];
        $array['exported_to_processo'] = $input['exported_to_processo'];
        
        if(empty($input['id'])){
            $details = Cilindros::firstOrCreate($array);
        }else{
            $isExist = Cilindros::find($input['id']);            
            if(!empty($isExist)){
                $details = Cilindros::updateOrCreate(['id' => $input['id']],$array);
            }else{
                return Response::json([
                    'status' =>  601,
                    'error' => "Cilindros details not exists."
                ],200);
            }
        }

        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $details = Cilindros::find($id);

        if(empty($details->id)){
            return Response::json([
                'status' =>  601,
                'error' => "Cilindros details not exists."
            ],200);
        }
        return Response::json([
			'status' => 200,
			'results' => $details
		],200);
    }

    /* Date Format */

    public function changeDateFormat($date){
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function displayDateFormat($date){
        return Carbon::parse($date)->format('d-m-Y');
    }
}
