<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendanceVariable extends Model
{
    protected $table = 'attendance_variables'; 
    protected $fillable = [
        'att_period_id', 'shifts_mapping_id', 'employee_id', 'employee_code', 'employee_name', 'ruta_fija', 'ruta_fija_code', 'puesto', 'att_date', 'ruta_code', 'actividad_code', 'incidencia_code', 'other_shift_code', 'oth_shift_actividad_code', 'is_freezed', 'created_by', 'updated_by'
    ];
}
