<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ausentismo extends Model
{
    protected $table = 'ausentismos'; 
    protected $fillable = [
        'user_id', 'employee_id', 'employee_name', 'start_date', 'ausentismo_type', 'hours', 'minutes', 'comments', 'created_by', 'updated_by'
    ];
	
	protected $casts = [
		'start_date' => 'date:d-m-Y',
	];
}
