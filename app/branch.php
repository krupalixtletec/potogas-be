<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table = 'branches'; 
    protected $fillable = [
        'branch_code', 'branch_name'
    ];
    public function shifts(){
        return $this->hasMany('App\shiftBranchMapping');
    }
}
