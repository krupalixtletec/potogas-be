<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendancePeriod extends Model
{
    protected $table = 'attendance_periods'; 
    protected $fillable = [
        'period_start_date', 'period_end_date', 'is_period_close', 'type', 'branch_id', 'created_by', 'period_key'
    ];

    public function attendanceFijos(){
        return $this->hasMany('App\attendanceFijos');
    }
    public function attendanceVariable(){
        return $this->hasMany('App\attendanceVariable');
    }
    public function shifts(){
        return $this->hasMany('App\shifts');
    }

    public function branch(){
        return $this->belongsTo('App\branch');
    }
}
