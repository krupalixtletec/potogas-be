<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usersRoleMapping extends Model
{
    protected $table = 'users_role_mapping'; 
    protected $fillable = [
        'user_id', 'role_id'
    ];
}
