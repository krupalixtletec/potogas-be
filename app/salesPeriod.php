<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesPeriod extends Model
{
    protected $table = 'sales_period'; 
    protected $fillable = [
        'period_start_date', 'period_end_date', 'is_period_close', 'type', 'branch_id', 'created_by', 'period_key'
    ];

    public function litrosSalesDetails(){
        return $this->hasMany('App\litrosSalesDetails');
    }

    public function cylinderSalesDetails(){
        return $this->hasMany('App\cylinderSalesDetails');
    }
    
    public function servicosSalesDetails(){
        return $this->hasMany('App\servicosSalesDetails');
    }
    public function shifts(){
        return $this->hasMany('App\shifts');
    }

    public function branch(){
        return $this->belongsTo('App\branch');
    }
}
