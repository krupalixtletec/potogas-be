<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shiftBranchMapping extends Model
{
    protected $table = 'shift_branch_mappings'; 
    protected $fillable = [
        'branch_id', 'shifts_id'
    ];

    public function shift(){
        return $this->belongsTo('App\shifts','shifts_id','id');
    }

    public function branch(){
        return $this->belongsTo('App\branch');
    }

    public function cylinderSalesDetails(){
        return $this->hasMany('App\cylinderSalesDetails','shifts_mapping_id','id');
    }

    public function litrosSalesDetails(){
        return $this->hasMany('App\litrosSalesDetails','shifts_mapping_id','id');
    }

    public function servicosSalesDetails(){
        return $this->hasMany('App\servicosSalesDetails','shifts_mapping_id','id');
    }
    
    public function attendanceFijos(){
        return $this->hasMany('App\attendanceFijos','shifts_mapping_id','id');
    }
    public function attendanceVariable(){
        return $this->hasMany('App\attendanceVariable','shifts_mapping_id','id');
    }
}
