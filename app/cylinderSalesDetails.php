<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cylinderSalesDetails extends Model
{
    protected $table = 'cylinder_sales_details'; 
    protected $fillable = [
        'shifts_id', 'sales_period_id', 'sale_date', 'ruta', 'ruta_code', 'sale_val', 'is_freezed', 'created_by', 'updated_by'
    ];

    public function store(){
        return $this->belongsTo('App\salesPeriod');
    }
}
