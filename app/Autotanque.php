<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autotanque extends Model
{
    protected $table = 'autotanques'; 
    protected $fillable = [
        'user_id', 'employee_id', 'employee_name', 'date', 'route_code', 'activity_code', 'branch_code', 'truck_number', 'destajo_type', 'liters', 'service_activity_code', 'services', 'parent_ID', 'exported_to_processo', 'created_by', 'updated_by'
    ];
    
	protected $casts = [
		'date' => 'date:d-m-Y',
	];
	
    public function helperDetail(){
        return $this->belongsTo('App\Autotanque','id','parent_ID');
    }
}
