<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shifts extends Model
{
    protected $table = 'shifts'; 
    protected $fillable = [
        'branch_id', 'shift', 'shift_code',
    ];

    //protected $primaryKey = array('sales_period_id','branch_id');

    public function store(){
        return $this->belongsTo('App\salesPeriod');
    }
    
    public function shift(){
        return $this->hasMany('App\shiftBranchMapping','shifts_id','id');
    }

}
