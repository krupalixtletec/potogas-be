<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usersRole extends Model
{
    protected $table = 'users_roles'; 
    protected $fillable = [
        'id','role_code', 'role_name'
    ];
}
