<?php

use GuzzleHttp\Client;
use App\userBranchMapping;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use App\salesPeriod;
use App\litrosSalesDetails;
use App\cylinderSalesDetails;
use App\servicosSalesDetails;
use App\usersRoleMapping;
use App\branch;
use App\shifts;
use App\users;
use App\usersRole;
use App\attendancePeriod;
use App\attendanceFijos;
use App\attendanceVariable;
use App\rolesClaims;
use App\shiftBranchMapping;

class Helper
{
    // Get Branch list by user
    static function getUserBrach($userID){
      $branch = userBranchMapping::join('branches', 'user_branch_mappings.branch_id', '=', 'branches.id' )
        ->where('user_id',$userID)
        ->get();
      return $branch;
    }
    static function getBranchs()
    {
      return branch::all();
    }
    static function updateUser()
    {
    }
    static function getUsers()
    {
      $users = users::all();
      foreach($users as $user)
      {
        $user['branchs'] = userBranchMapping::join('branches','user_branch_mappings.branch_id','=','branches.id')
        ->where('user_branch_mappings.user_id',$user['user_id'])
        ->select('branches.branch_code')->pluck('branch_code');
        $user['rols'] = usersRoleMapping::join('users_roles','users_role_mapping.role_id','=','users_roles.id')
        ->where('users_role_mapping.user_id',$user['user_id'])
        ->select('users_roles.role_code')->pluck('role_code');
      }
      return $users;
    }
    static function updateMappingBranchs($branchs, $user_id)
    {
      userBranchMapping::where('user_id','=',$user_id)->delete();
      foreach($branchs as $branch)
      {
        $branchDB = branch::where('branch_code','=',$branch)->first();
        userBranchMapping::insert(['user_id'=>$user_id,'branch_id'=>$branchDB->id]);
      }
    }
    static function updateMappingRols($rols, $user_id)
    {
      usersRoleMapping::where('user_id','=',$user_id)->delete();
      foreach($rols as $rol)
      {
        $rolDB = usersRole::where('role_code','=',$rol)->first();
        usersRoleMapping::insert(['user_id'=>$user_id,'role_id'=>$rolDB->id]);
      }
    }
    static function getRols()
    {
      $roles= usersRole::all();
      foreach($roles as $rol)
      {
        $rol['Claims'] = rolesClaims::where('role_id','=',$rol['id'])->pluck('claim');
      }
      return $roles;
    }
    static function getClaims($idRol)
    {
      $claims = rolesClaims::where('role_id','=',$idRol)->pluck('claim');
      return $claims;
    }
    static function insertUser($email)
    {
      $user = users::where('email','=',$email)->first();
      if(!isset($user))
      {
        $id = users::insertGetId(['name'=>$email, 'email'=>$email, 'user_id'=>3]);
        users::where('id','=',$id)->update(['user_id'=>$id]);  
      }
    }
    static function setUser($payload)
    {
      $user = users::where('email','=',$payload['email'])->first();
      if(!isset($user))
      {
        $id = users::insertGetId(['name'=>$payload['name'], 'email'=>$payload['email'], 'user_id'=>3]);
        users::where('id','=',$id)->update(['user_id'=>$id]);  
      }
    }
    static function loginByLocal($username, $password)
    {
      $user = users::where('email','=',$username)->where('password','=',$password)->first();
      if(isset($user))
      {
        $id = $user->id;
      }
      else
      {
        $id = null;
      }
      return $id;
    }
    static function getIdByEmail($email)
    {
      $user = users::firstWhere('email','=',$email);
      if(isset($user))
      {
        $id = $user->id;
      }
      else
      {
        $id = null;
      }
      return $id;
    }
    static function getClaimsByEmail($email)
    {
      $id = users::firstWhere('email','=',$email)->id;
      $claims = null;
      if(isset($id))
      {
        $idRol = usersRoleMapping::firstWhere('user_id','=',$id);
        if(isset($idRol))
        {
          $claims = Helper::getClaims($idRol->role_id);
        }
        return $claims;
      }
      return null;
    }
    static function deleteRol($id)
    {
      usersRole::where('id','=',$id)->delete();
    }
    static function updateRols($id, $name, $claims)
    {
      if(isset($id))
      {
        usersRole::where('id','=',$id)->update(['role_name'=>$name]);
      }
      else
      {
        $id = usersRole::insertGetId(['role_name'=>$name, 'role_code'=>$name]);
      }
      rolesClaims::where('role_id','=',$id)->delete();
      foreach($claims as $claim)
      {
        rolesClaims::insert(['role_id'=>$id, 'claim'=>$claim]);
      }
    }

    static function UserIdByEmail($email){
      $data = users::where('email',$email)->first();
      return $data;
    }

    static function UserRoleById($user_id){
      $roleData = usersRole::join('users_role_mapping', 'users_role_mapping.role_id', '=', 'users_roles.id')->where('user_id',$user_id)->first();
      return $roleData;
    }

    // Litros Details Start
    static function insertBranchDetails($branch){
      $returnIds = array();
      $existBranch = array();
      $empresa = env("EMPRESA"); 
      $API_HOST_URL = env("API_HOST_URL"); 
      
      $collection = collect($branch)->map(function ($details) {
          return $details['branch_code'];
      });
      
      $collection = array_unique($collection->toArray(), SORT_REGULAR);
      
      $client = new Client([
          'headers' => [ 'Empresa' => $empresa,'Content-Type' => 'application/json' ]
      ]);

      $response = $client->post("$API_HOST_URL/api/Period/GetCurrentPeriods",
          ['body' => json_encode($collection)]
      );
      $statusCode = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      $branchResponse = json_decode($body);
      
      if(!empty($branchResponse->Result) && is_array($branchResponse->Result)){
        foreach($branchResponse->Result as $key => $response){            
          $branchId = $branch->firstWhere('branch_code',$response->BranchCode)->id;
          if(!empty($response->Periods) && is_array($response->Periods)){
            foreach($response->Periods as $key => $periodsResponse){
              
              $array['period_start_date'] = Helper::insertDateFormat($periodsResponse->PeriodBeginDate);
              $array['period_end_date'] = Helper::insertDateFormat($periodsResponse->PeriodEndDate);
              $array['branch_id'] =  $branchId;
              $array['is_period_close'] = 0;  
              $array['period_key'] = $periodsResponse->PeriodCode;
              $array['type'] = 'Au';
          
              $isExist = salesPeriod::where($array)->first(); 
              if(!empty($isExist)){
                array_push($existBranch,$response->BranchCode);
                array_push($returnIds,$isExist->id);
              }else{
                $sales = salesPeriod::firstOrCreate($array);
                array_push($returnIds,$sales->id);
              }
                    
            }
          } 
        }
      }else{
        $salesIds = salesPeriod::where('type','Au')->where('period_start_date', '<=', date('Y-m-d'))->where('period_end_date', '>=', date('Y-m-d'))->pluck('id')->toArray();
        if(!empty($salesIds)){
          return $salesIds;
        }else{
          $salesIds = salesPeriod::where('type','Au')->where('period_end_date', '<=', date('Y-m-d'))->orderBy('period_end_date','desc')->take(1)->pluck('id')->toArray();
          if(!empty($salesIds)){
            return $salesIds;
          }
        }
      }
      

      $detailResponse = $client->post("$API_HOST_URL/api/Route/GetRoutes?productKey=Au",
          ['body' => json_encode($collection)]
      );
      $detailStatusCode = $detailResponse->getStatusCode();
      $detailBody = $detailResponse->getBody()->getContents();
      $detailBranchResponse = json_decode($detailBody);

      if($detailStatusCode == 200){
        if(!empty($detailBranchResponse->Result) && is_array($detailBranchResponse->Result)){          
          foreach($detailBranchResponse->Result as $key => $shiftResponse){

            if(!in_array($shiftResponse->BranchCode,$existBranch)){
              $detailsData = array();
              foreach($shiftResponse->Shifts as $shiftDetails){    
              $masterData =  branch::join('sales_period', 'sales_period.branch_id', '=', 'branches.id' )
                ->where('branch_code',$shiftResponse->BranchCode)
                ->where('type','Au')->first();
                
                if(!empty($masterData)){
                  
                  $shiftarray['shift'] = $shiftDetails->ShiftsName;
                  $shiftarray['shift_code'] = $shiftDetails->ShiftsCode;
              
                  $shift = shifts::firstOrCreate($shiftarray);

                  $shiftMappingarray['branch_id'] = $masterData->branch_id;
                  $shiftMappingarray['shifts_id'] = $shift->id;
                  
                  $mapping = shiftBranchMapping::firstOrCreate($shiftMappingarray);

                  foreach($shiftDetails->Routes as $routeDetails){
                    $period = CarbonPeriod::create($masterData->period_start_date,$masterData->period_end_date);
                    foreach ($period as $date) {
                      $detailsData[] =  [ 'shifts_mapping_id' => $mapping->id, 'sales_period_id'=> $masterData->id, 'sale_date' => $date->format('Y-m-d'), 'ruta' => $routeDetails->RouteName, 'ruta_code' => $routeDetails->RouteCode, 'sale_val' => 0, 'created_at' => Carbon::now()];
                      
                    }
                  } 
                }
              }
              $noOfArray = ceil(count($detailsData)/100);
              if($noOfArray > 0){
                $insertData = array_chunk($detailsData,$noOfArray);
                foreach($insertData as $details){
                  litrosSalesDetails::insert($details);
                }
              }else{
                litrosSalesDetails::insert($detailsData);
              }
            }
          }          
        }
    }
      return array_unique($returnIds);
    }
    // Litros Details End

    // Cylinder Sales Details Start
    static function insertCylinderDetails($branch){
      $returnIds = array();
      $existBranch = array();
      $empresa = env("EMPRESA"); 
      $API_HOST_URL = env("API_HOST_URL"); 
      
      $collection = collect($branch)->map(function ($details) {
          return $details['branch_code'];
      });
      
      $collection = array_unique($collection->toArray(), SORT_REGULAR);
      
      $client = new Client([
          'headers' => [ 'Empresa' => $empresa,'Content-Type' => 'application/json' ]
      ]);

      $response = $client->post("$API_HOST_URL/api/Period/GetCurrentPeriods",
          ['body' => json_encode($collection)]
      );
      $statusCode = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      $branchResponse = json_decode($body);
      
      if(!empty($branchResponse->Result) && is_array($branchResponse->Result)){
        foreach($branchResponse->Result as $key => $response){            
          $branchId = $branch->firstWhere('branch_code',$response->BranchCode)->id;
          if(!empty($response->Periods) && is_array($response->Periods)){
            foreach($response->Periods as $key => $periodsResponse){
              $array['period_start_date'] = Helper::insertDateFormat($periodsResponse->PeriodBeginDate);
              $array['period_end_date'] = Helper::insertDateFormat($periodsResponse->PeriodEndDate);
              $array['branch_id'] =  $branchId;
              $array['is_period_close'] = 0;  
              $array['period_key'] = $periodsResponse->PeriodCode;
              $array['type'] = 'Ci';

              $isExist = salesPeriod::where($array)->first(); 
              if(!empty($isExist)){
                array_push($existBranch,$response->BranchCode);
                array_push($returnIds,$isExist->id);
              }else{
                $sales = salesPeriod::firstOrCreate($array);
                array_push($returnIds,$sales->id);    
              }  
            }
          }     
        }
      }else{
        $salesIds = salesPeriod::where('type','Ci')->where('period_start_date', '<=', date('Y-m-d'))->where('period_end_date', '>=', date('Y-m-d'))->pluck('id')->toArray();
        if(!empty($salesIds)){
          return $salesIds;
        }else{
          $salesIds = salesPeriod::where('type','Ci')->where('period_end_date', '<=', date('Y-m-d'))->orderBy('period_end_date','desc')->take(1)->pluck('id')->toArray();
          if(!empty($salesIds)){
            return $salesIds;
          }
        }
      }

      $detailResponse = $client->post("$API_HOST_URL/api/Route/GetRoutes?productKey=Ci",
          ['body' => json_encode($collection)]
      );
      $detailStatusCode = $detailResponse->getStatusCode();
      $detailBody = $detailResponse->getBody()->getContents();
      $detailBranchResponse = json_decode($detailBody);

      if($detailStatusCode == 200){
        if(!empty($detailBranchResponse->Result) && is_array($detailBranchResponse->Result)){
          foreach($detailBranchResponse->Result as $key => $shiftResponse){
            if(!in_array($shiftResponse->BranchCode,$existBranch)){
              $detailsData = array();
              foreach($shiftResponse->Shifts as $shiftDetails){  
              $masterData =  branch::join('sales_period', 'sales_period.branch_id', '=', 'branches.id' )
                ->where('branch_code',$shiftResponse->BranchCode)
                ->where('type','Ci')->first();
                
             if(!empty($masterData)){
                  
                  $shiftarray['shift'] = $shiftDetails->ShiftsName;
                  $shiftarray['shift_code'] = $shiftDetails->ShiftsCode;
              
                  $shift = shifts::firstOrCreate($shiftarray);

                  $shiftMappingarray['branch_id'] = $masterData->branch_id;
                  $shiftMappingarray['shifts_id'] = $shift->id;
                  
                  $mapping = shiftBranchMapping::firstOrCreate($shiftMappingarray);

                  foreach($shiftDetails->Routes as $routeDetails){
                    $period = CarbonPeriod::create($masterData->period_start_date,$masterData->period_end_date);
                    foreach ($period as $date) {
                      $detailsData[] = [ 'shifts_mapping_id' => $mapping->id, 'sales_period_id'=> $masterData->id, 'sale_date' => $date->format('Y-m-d'), 'ruta' => $routeDetails->RouteName, 'ruta_code' => $routeDetails->RouteCode, 'sale_val' => 0, 'created_at' => Carbon::now()];
                    }
                  } 
                }
              }
              $noOfArray = ceil(count($detailsData)/100);
              if($noOfArray > 0){
                $insertData = array_chunk($detailsData,$noOfArray);
                foreach($insertData as $details){
                  cylinderSalesDetails::insert($details);
                }
              }else{
                cylinderSalesDetails::insert($detailsData);
              }
            }
          }
        }
      }
      return array_unique($returnIds);
    }
    // Cylinder Sales Details End

     // Servicos Details Start
     static function insertServicosDetails($branch){
      $returnIds = array();
      $existBranch = array();
      $empresa = env("EMPRESA"); 
      $API_HOST_URL = env("API_HOST_URL"); 
      
      $collection = collect($branch)->map(function ($details) {
          return $details['branch_code'];
      });
      
      $collection = array_unique($collection->toArray(), SORT_REGULAR);
      
      $client = new Client([
          'headers' => [ 'Empresa' => $empresa,'Content-Type' => 'application/json' ]
      ]);

      $response = $client->post("$API_HOST_URL/api/Period/GetCurrentPeriods",
          ['body' => json_encode($collection)]
      );
      $statusCode = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      $branchResponse = json_decode($body);
      
    
      if(!empty($branchResponse->Result) && is_array($branchResponse->Result)){
        foreach($branchResponse->Result as $key => $response){
          $branchId = $branch->firstWhere('branch_code',$response->BranchCode)->id;
          if(!empty($response->Periods) && is_array($response->Periods)){
            foreach($response->Periods as $key => $periodsResponse){
              $array['period_start_date'] = Helper::insertDateFormat($periodsResponse->PeriodBeginDate);
              $array['period_end_date'] = Helper::insertDateFormat($periodsResponse->PeriodEndDate);
              $array['branch_id'] =  $branchId;
              $array['is_period_close'] = 0;  
              $array['period_key'] = $periodsResponse->PeriodCode;
              $array['type'] = 'Se';
          
              $isExist = salesPeriod::where($array)->first(); 
              if(!empty($isExist)){
                array_push($existBranch,$response->BranchCode);
                array_push($returnIds,$isExist->id);
              }else{
                $sales = salesPeriod::firstOrCreate($array);
                array_push($returnIds,$sales->id);      
              }
            }
          }      
        }
      }else{
        $salesIds = salesPeriod::where('type','Se')->where('period_start_date', '<=', date('Y-m-d'))->where('period_end_date', '>=', date('Y-m-d'))->pluck('id')->toArray();
        if(!empty($salesIds)){
          return $salesIds;
        }else{
          $salesIds = salesPeriod::where('type','Se')->where('period_end_date', '<=', date('Y-m-d'))->orderBy('period_end_date','desc')->take(1)->pluck('id')->toArray();
          if(!empty($salesIds)){
            return $salesIds;
          }
        }
      }

      $detailResponse = $client->post("$API_HOST_URL/api/Route/GetRoutes?productKey=Au",
          ['body' => json_encode($collection)]
      );
      $detailStatusCode = $detailResponse->getStatusCode();
      $detailBody = $detailResponse->getBody()->getContents();
      $detailBranchResponse = json_decode($detailBody);

      if($detailStatusCode == 200){
        if(!empty($detailBranchResponse->Result) && is_array($detailBranchResponse->Result)){
          foreach($detailBranchResponse->Result as $key => $shiftResponse){          
            if(!in_array($shiftResponse->BranchCode,$existBranch)){
              $detailsData = array();
              foreach($shiftResponse->Shifts as $shiftDetails){    

              $masterData =  branch::join('sales_period', 'sales_period.branch_id', '=', 'branches.id' )
                ->where('branch_code',$shiftResponse->BranchCode)
                ->where('type','Se')->first();
                
                 if(!empty($masterData)){
                  
                  $shiftarray['shift'] = $shiftDetails->ShiftsName;
                  $shiftarray['shift_code'] = $shiftDetails->ShiftsCode;
              
                  $shift = shifts::firstOrCreate($shiftarray);
                  
                  $shiftMappingarray['branch_id'] = $masterData->branch_id;
                  $shiftMappingarray['shifts_id'] = $shift->id;
                  
                  $mapping = shiftBranchMapping::firstOrCreate($shiftMappingarray);

                  foreach($shiftDetails->Routes as $routeDetails){
                    $period = CarbonPeriod::create($masterData->period_start_date,$masterData->period_end_date);
                    foreach ($period as $date) {
                      $detailsData[] =  [ 'shifts_mapping_id' => $mapping->id, 'sales_period_id'=> $masterData->id, 'sale_date' => $date->format('Y-m-d'), 'ruta' => $routeDetails->RouteName, 'ruta_code' => $routeDetails->RouteCode, 'sale_val' => 0, 'created_at' => Carbon::now()];
                    }
                  } 
                }
              }
              $noOfArray = ceil(count($detailsData)/100);
              if($noOfArray > 0){
                $insertData = array_chunk($detailsData,$noOfArray);
                foreach($insertData as $details){
                  servicosSalesDetails::insert($details);
                }
              }else{
                servicosSalesDetails::insert($detailsData);
              }
            }
          }
        }
      }
      return array_unique($returnIds);
    }
    // Servicos Details End


    // Attendance fijos details Start
    static function insertAttendanceFijosDetails($branch){
      $returnIds = array();
      $existBranch = array();
      $empresa = env("EMPRESA"); 
      $API_HOST_URL = env("API_HOST_URL"); 
      
      $collection = collect($branch)->map(function ($details) {
          return $details['branch_code'];
      });
      
      $collection = array_unique($collection->toArray(), SORT_REGULAR);
      
      $client = new Client([
          'headers' => [ 'Empresa' => $empresa,'Content-Type' => 'application/json' ]
      ]);

      $response = $client->post("$API_HOST_URL/api/Period/GetCurrentPeriods",
          ['body' => json_encode($collection)]
      );
      $statusCode = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      $branchResponse = json_decode($body);
      
     
      if(!empty($branchResponse->Result) && is_array($branchResponse->Result)){
        foreach($branchResponse->Result as $key => $response){            
          $branchId = $branch->firstWhere('branch_code',$response->BranchCode)->id;
          if(!empty($response->Periods) && is_array($response->Periods)){
            foreach($response->Periods as $key => $periodsResponse){
              $array['period_start_date'] = Helper::insertDateFormat($periodsResponse->PeriodBeginDate);
              $array['period_end_date'] = Helper::insertDateFormat($periodsResponse->PeriodEndDate);
              $array['branch_id'] =  $branchId;
              $array['is_period_close'] = 0;  
              $array['period_key'] = $periodsResponse->PeriodCode;
              $array['type'] = 'F';
          
              $isExist = attendancePeriod::where($array)->first(); 
              if(!empty($isExist)){
                array_push($existBranch,$response->BranchCode);
                array_push($returnIds,$isExist->id);
              }else{
                $sales = attendancePeriod::firstOrCreate($array);
                array_push($returnIds,$sales->id); 
              }
            }
          }   
        }
      }else{
        $salesIds = attendancePeriod::where('type','F')->where('period_start_date', '<=', date('Y-m-d'))->where('period_end_date', '>=', date('Y-m-d'))->pluck('id')->toArray();
        if(!empty($salesIds)){
          return $salesIds;
        }else{
          $salesIds = attendancePeriod::where('type','F')->where('period_end_date', '<=', date('Y-m-d'))->orderBy('period_end_date','desc')->take(1)->pluck('id')->toArray();
          if(!empty($salesIds)){
            return $salesIds;
          }
        }
      }
      

      $detailResponse = $client->post("$API_HOST_URL/api/EmployeeCatalog/GetEmployees?type=F",
          ['body' => json_encode($collection)]
      );
      $detailStatusCode = $detailResponse->getStatusCode();
      $detailBody = $detailResponse->getBody()->getContents();
      $detailBranchResponse = json_decode($detailBody);

      if($detailStatusCode == 200){
        if(!empty($detailBranchResponse->Result) && is_array($detailBranchResponse->Result)){
          foreach($detailBranchResponse->Result as $key => $shiftResponse){
            if(!in_array($shiftResponse->BranchCode,$existBranch)){
              $detailsData = array();
              foreach($shiftResponse->Shifts as $shiftDetails){    

              $masterData =  branch::join('attendance_periods', 'attendance_periods.branch_id', '=', 'branches.id' )
                ->where('branch_code',$shiftResponse->BranchCode)
                ->where('type','F')->first();
                
                 if(!empty($masterData)){
                  
                  $shiftarray['shift'] = $shiftDetails->ShiftsName;
                  $shiftarray['shift_code'] = $shiftDetails->ShiftsCode;
              
                  $shift = shifts::firstOrCreate($shiftarray);

                  $shiftMappingarray['branch_id'] = $masterData->branch_id;
                  $shiftMappingarray['shifts_id'] = $shift->id;
                  
                  $mapping = shiftBranchMapping::firstOrCreate($shiftMappingarray);

                  foreach($shiftDetails->Employees as $emp){
                    $period = CarbonPeriod::create($masterData->period_start_date,$masterData->period_end_date);
                    foreach ($period as $date) {
                      $detailsData[] = [ 'shifts_mapping_id' => $mapping->id, 'att_period_id'=> $masterData->id, 'att_date' => $date->format('Y-m-d'), 'employee_id' => $emp->EmpId, 'employee_code' => $emp->EmpCode, 'employee_name' => $emp->EmpName, 'puesto' => $emp->Job, 'created_at' => Carbon::now()];                      
                    }
                  } 
                }
              }
              $noOfArray = ceil(count($detailsData)/100);
              if($noOfArray > 0){
                $insertData = array_chunk($detailsData,$noOfArray);
                foreach($insertData as $details){
                  attendanceFijos::insert($details);
                }
              }else{
                attendanceFijos::insert($detailsData);
              }
            }
          }
        }
      }
      return array_unique($returnIds);
    }    
    // Attendance fijos details End


     // Attendance Variable details Start
     static function insertAttendanceVariableDetails($branch){
      $returnIds = array();
      $existBranch = array();
      $empresa = env("EMPRESA"); 
      $API_HOST_URL = env("API_HOST_URL"); 
      
      $collection = collect($branch)->map(function ($details) {
          return $details['branch_code'];
      });
      
      $collection = array_unique($collection->toArray(), SORT_REGULAR);
      
      $client = new Client([
          'headers' => [ 'Empresa' => $empresa,'Content-Type' => 'application/json' ]
      ]);

      $response = $client->post("$API_HOST_URL/api/Period/GetCurrentPeriods",
          ['body' => json_encode($collection)]
      );
      $statusCode = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      $branchResponse = json_decode($body);
      
      if(!empty($branchResponse->Result) && is_array($branchResponse->Result)){
        foreach($branchResponse->Result as $key => $response){            
          $branchId = $branch->firstWhere('branch_code',$response->BranchCode)->id;
          if(!empty($response->Periods) && is_array($response->Periods)){
            foreach($response->Periods as $key => $periodsResponse){
              $array['period_start_date'] = Helper::insertDateFormat($periodsResponse->PeriodBeginDate);
              $array['period_end_date'] = Helper::insertDateFormat($periodsResponse->PeriodEndDate);
              $array['branch_id'] =  $branchId;
              $array['is_period_close'] = 0;  
              $array['period_key'] = $periodsResponse->PeriodCode;
              $array['type'] = 'V';
          
              $isExist = attendancePeriod::where($array)->first(); 
              if(!empty($isExist)){
                array_push($existBranch,$response->BranchCode);
                array_push($returnIds,$isExist->id);
              }else{
                $sales = attendancePeriod::firstOrCreate($array);
                array_push($returnIds,$sales->id); 
              }
            }
          }   
        }
      }else{
        $salesIds = attendancePeriod::where('type','V')->where('period_start_date', '<=', date('Y-m-d'))->where('period_end_date', '>=', date('Y-m-d'))->pluck('id')->toArray();
        if(!empty($salesIds)){
          return $salesIds;
        }else{
          $salesIds = attendancePeriod::where('type','V')->where('period_end_date', '<=', date('Y-m-d'))->orderBy('period_end_date','desc')->take(1)->pluck('id')->toArray();
          if(!empty($salesIds)){
            return $salesIds;
          }
        }
      }
      

      $detailResponse = $client->post("$API_HOST_URL/api/EmployeeCatalog/GetEmployees?type=V",
          ['body' => json_encode($collection)]
      );
      $detailStatusCode = $detailResponse->getStatusCode();
      $detailBody = $detailResponse->getBody()->getContents();
      $detailBranchResponse = json_decode($detailBody);

      if($detailStatusCode == 200){
        if(!empty($detailBranchResponse->Result) && is_array($detailBranchResponse->Result)){
          foreach($detailBranchResponse->Result as $key => $shiftResponse){
            if(!in_array($shiftResponse->BranchCode,$existBranch)){
              $detailsData = array();
              foreach($shiftResponse->Shifts as $shiftDetails){    

              $masterData =  branch::join('attendance_periods', 'attendance_periods.branch_id', '=', 'branches.id' )
                ->where('branch_code',$shiftResponse->BranchCode)
                ->where('type','V')->first();
                
                 if(!empty($masterData)){
                  
                  $shiftarray['shift'] = $shiftDetails->ShiftsName;
                  $shiftarray['shift_code'] = $shiftDetails->ShiftsCode;
              
                  $shift = shifts::firstOrCreate($shiftarray);

                  $shiftMappingarray['branch_id'] = $masterData->branch_id;
                  $shiftMappingarray['shifts_id'] = $shift->id;
                  
                  $mapping = shiftBranchMapping::firstOrCreate($shiftMappingarray);

                  foreach($shiftDetails->Employees as $emp){
                    $period = CarbonPeriod::create($masterData->period_start_date,$masterData->period_end_date);
                    foreach ($period as $date) {
                       $detailsData[] = [ 'shifts_mapping_id' => $mapping->id, 'att_period_id'=> $masterData->id, 'att_date' => $date->format('Y-m-d'), 'employee_id' => $emp->EmpId, 'employee_code' => $emp->EmpCode, 'employee_name' => $emp->EmpName, 'puesto' => $emp->Job, 'created_at' => Carbon::now()];
                    }
                  } 
                }
              }
              $noOfArray = ceil(count($detailsData)/100);
              if($noOfArray > 0){
                $insertData = array_chunk($detailsData,$noOfArray);
                foreach($insertData as $details){
                  attendanceVariable::insert($details);
                }
              }else{
                attendanceVariable::insert($detailsData);
              }
            }
          }
          // Insert Data 
        }
      }
      return array_unique($returnIds);
    }    
    // Attendance Variable details End

    // Change date format 
    static function insertDateFormat($date){
      return Carbon::parse($date)->format('Y-m-d');
    }
}