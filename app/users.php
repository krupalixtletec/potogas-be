<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'users'; 
    protected $fillable = [
        'id','user_id', 'name', 'email', 'email_verified_at', 'password', 'remember_token'
    ];

}
