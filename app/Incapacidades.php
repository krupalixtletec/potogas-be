<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incapacidades extends Model
{
    protected $table = 'incapacidades'; 
    protected $fillable = [
        'user_id', 'employee_id', 'employee_name', 'numero_de_folio', 'start_date', 'discount_date', 'duration', 'rama', 'risk_type', 'consecuence', 'control', 'date_of_accident', 'place_of_accident', 'accident_cause', 'type_of_injury', 'corrective_measure', 'day', 'time', 'created_by', 'updated_by'
    ];
	
	protected $casts = [
		'start_date' => 'date:d-m-Y',
		'discount_date' => 'date:d-m-Y',
		'date_of_accident' => 'date:d-m-Y',
	];
}
