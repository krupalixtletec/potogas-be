<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rolesClaims extends Model
{
    protected $table = 'roles_claims'; 
    protected $fillable = [
        'id','role_id', 'claim'
    ];

}
