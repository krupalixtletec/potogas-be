<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cilindros extends Model
{
    protected $table = 'cilindros'; 
    protected $fillable = [
        'user_id', 'employee_id', 'employee_name', 'date', 'route_code', 'activity_code', 'branch_code', 'truck_number', 'destajo_type', 'capture_kilos', 'number_of_cilindros', 'exported_to_processo', 'created_by', 'updated_by'
    ];
	
	protected $casts = [
		'date' => 'date:d-m-Y',
	];
}
