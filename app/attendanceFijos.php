<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendanceFijos extends Model
{
    protected $table = 'attendance_fijos'; 
    protected $fillable = [
        'att_period_id', 'shifts_id', 'employee_id', 'employee_code', 'employee_name', 'puesto', 'att_date', 'incidencia_code', 'tiempo_extra', 'viaticos', 'is_freezed', 'created_by', 'updated_by'
    ];
}
