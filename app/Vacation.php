<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
    protected $table = 'vacations'; 
    protected $fillable = [
        'user_id', 'employee_id', 'employee_name', 'start_date', 'duration', 'payment_date', 'discanso_1', 'discanso_2', 'created_by', 'updated_by'
    ];
	
	protected $casts = [
		'start_date' => 'date:d-m-Y',
		'payment_date' => 'datetime:d-m-Y',
	];
}
