<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userBranchMapping extends Model
{
    protected $table = 'user_branch_mappings'; 
    protected $fillable = [
        'user_id', 'branch_id'
    ];

    public function users(){
        return $this->belongsTo('App\User');
    }
    public function branch(){
        return $this->belongsTo('App\branch');
    }
}
