<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('litrosdetails', 'LitrosSalesController@index');
Route::post('litrosdetails/insert', 'LitrosSalesController@insert');
Route::post('litrosdetails/user_branch', 'LitrosSalesController@user_branch');

Route::post('cylinderdetails', 'cylinderDetailsController@index');
Route::post('cylinderdetails/insert', 'cylinderDetailsController@insert');
Route::post('cylinderdetails/user_branch', 'cylinderDetailsController@user_branch');

Route::post('servicosdetails', 'servicosDetailsController@index');
Route::post('servicosdetails/insert', 'servicosDetailsController@insert');
Route::post('servicosdetails/user_branch', 'servicosDetailsController@user_branch');

Route::post('attendancefijosdetails', 'attendanceFijosController@index');
Route::post('attendancefijosdetails/insert', 'attendanceFijosController@insert');
Route::post('attendancefijosdetails/user_branch', 'attendanceFijosController@user_branch');


Route::post('attendancevariabledetails', 'attendanceVariableController@index');
Route::post('attendancevariabledetails/insert', 'attendanceVariableController@insert');
Route::post('attendancevariabledetails/user_branch', 'attendanceVariableController@user_branch');


Route::post('vacations/insert', 'VacationsController@insert');
Route::post('vacations', 'VacationsController@index');
Route::post('vacations/{id}', 'VacationsController@details');

Route::post('ausentismos/insert', 'AusentismoController@insert');
Route::post('ausentismos', 'AusentismoController@index');
Route::post('ausentismos/{id}', 'AusentismoController@details');

Route::post('incapacidades/insert', 'IncapacidadeController@insert');
Route::post('incapacidades', 'IncapacidadeController@index');
Route::post('incapacidades/{id}', 'IncapacidadeController@details');

Route::post('cilindros/insert', 'CilindrosController@insert');
Route::post('cilindros', 'CilindrosController@index');
Route::post('cilindros/{id}', 'CilindrosController@details');

Route::post('autotanques/insert', 'AutotanqueController@insert');
Route::post('autotanques', 'AutotanqueController@index');
Route::post('autotanques/{id}', 'AutotanqueController@details');

Route::get('test', function()
{
    return 'test';
});
Route::post('verifyToken', 'AccountController@verifyToken');
Route::get('getBranchs','PermissionsController@getBranchs');
Route::get('getUsers','PermissionsController@getUsers');
Route::get('getRols','PermissionsController@getRols');

Route::put('updateUser','PermissionsController@updateUser');
Route::post('updateRols','PermissionsController@updateRols');
Route::delete('deleteRol','PermissionsController@deleteRol');

Route::post('shiftdetails', 'shiftController@index');
Route::post('branchdetails', 'branchController@index');
Route::post('userbranchdetails', 'branchController@userbranchlist');

Route::post('login','AccountController@login');

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
 */